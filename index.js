var express=require('express');
var bodyParser=require('body-parser');
var sqlite3=require('sqlite3').verbose();
var uuidV1 = require('uuid/v1');
var cors=require('cors');
var db=new sqlite3.Database('../sqlitedesk.sqlite');
var app=express();
app.use(cors({origin: 'http://localhost:8080'}));
app.use(express.static('web'));

// get a list of tasks - no versions for PURPOSE as we simulate a legacy
app.get('/aufgaben', function (req, res){
	db.all("SELECT * FROM aufgabe ORDER BY uuid", function(err, rows){
		res.send(JSON.stringify(rows));
	});
});
app.post('/aufgaben', bodyParser.json(), function(req, res){
    var uuid=uuidV1();
	db.run("INSERT INTO aufgabe (uuid, title, done) VALUES (?, ?, ?)", uuid, req.body.title, (req.body.done?1:0), function(err){
		// TODO: Error-Handling
		res.send(JSON.stringify({ok: true}));
	});
});
app.put('/aufgaben/:id', bodyParser.json(), function(req, res){
  db.run("UPDATE aufgabe SET title=?, done=? where uuid=?", req.body.title, (req.body.done?1:0), req.params.id, function(err){
	// TODO: Error-Handling
	if(this.changes<1){
		// brauchen einen Insert; TODO: in Funktionen ziehen
		db.run("INSERT INTO aufgabe (uuid, title, done) VALUES (?, ?, ?)", req.params.id, req.body.title, (req.body.done?1:0), function(err){
			// TODO: Error-Handling
			res.send(JSON.stringify({ok: true}));
		});
	}else{
		res.send(JSON.stringify({ok: true}));
	}
  });
});
app.listen(8088);
